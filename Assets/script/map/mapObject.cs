﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mapObject : MonoBehaviour
{
    [SerializeField] 
    public Sprite sprite;
    [SerializeField]
    public GameObject owner;
    public Image icon;
    void Start()
    {
        owner = gameObject;

        GameManager.Instance().mc.RegisterObject(this);
    }


}
